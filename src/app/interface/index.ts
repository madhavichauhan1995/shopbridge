export interface Product {
    name: string,
    description: string,
    price: string,
    id?: number
}