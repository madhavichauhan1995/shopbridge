import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { UserService } from './user.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fn } from '@angular/compiler/src/output/output_ast';

const data = [
  {
    "id": 552,
    "name": "Sony Turntable - PSLX350H",
    "description": "Sony Turntable - PSLX350H/ Belt Drive System/ 33-1/3 and 45 RPM Speeds/ Servo Speed Control/ Supplied Moving Magnet Phono Cartridge/ Bonded Diamond Stylus/ Static Balance Tonearm/ Pitch Control",
    "price": "$399.00"
  }];

describe('UserService', () => {
  let service: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService]
    }).compileComponents();
    localStorage.setItem("ProductList", JSON.stringify(data))
    service = TestBed.get(UserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call getProduct', () => {
    service.productList = null;
    const getProductSpy = spyOn(service, "getProduct").and.callThrough()
    service.getProduct();
    expect(getProductSpy).toHaveBeenCalled();
  });

  it('should able to add product', () => {
    localStorage.setItem("ProductList", JSON.stringify(data))
    const addProductSpy = spyOn(service, "addOrEditProduct").and.callThrough()
    service.addOrEditProduct({ name: 'test', description: 'test', price: '100$' });
    expect(addProductSpy).toHaveBeenCalled();
  });

  it('should able to edit product', () => {
    localStorage.setItem("ProductList", JSON.stringify(data))
    const editProductSpy = spyOn(service, "addOrEditProduct").and.callThrough()
    service.addOrEditProduct({ name: 'test', description: 'test', price: '100' }, 552);
    expect(editProductSpy).toHaveBeenCalled();
  });

  it('should able to edit product', () => {
    const deleteProductSpy = spyOn(service, "deleteProduct")
    service.deleteProduct(123);
    expect(deleteProductSpy).toHaveBeenCalled();
  });
});
