import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, shareReplay, tap } from 'rxjs/operators';
import { Product } from '../interface';
import { promise } from 'protractor';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  constructor(private http: HttpClient) { }
  GET_PRODUCT_API_ENDPOINTS = 'assets/mock-data/data.json';
  ADD_OR_EDIT_PRODUCT_API_ENDPOINTS = 'assets/mock-data/success.json'
  productList = null;

  /**
   * @method
   * @name getProduct
   * @description used to get Product list
   * @return  Promise<object> 
   */
  getProduct(): Promise<object> {
    if (JSON.parse(localStorage.getItem("ProductList")) === null) {
      return new Promise((resolve, reject) => {
        this.http
          .get(this.GET_PRODUCT_API_ENDPOINTS)
          .pipe(
            shareReplay(),
            map((response) => response),
          )
          .toPromise()
          .then((res) => {
            localStorage.setItem('ProductList', JSON.stringify(res));
            resolve(res)
          })
          .catch((err) => reject(err));
      });
    }
    else {
      return new Promise((resolve) => {
        resolve(JSON.parse(localStorage.getItem("ProductList")))
      });
    }

  }

  /**
  * @method
  * @name addOrEditProduct
  * @description used to edit specific product.
  * @return  Promise<object> 
  * @param <Product> product - hold edit product object
  * @param <string> id - hold edit product id 
  */
  addOrEditProduct(product: Product, id?: number): Promise<object> {
    this.productList = JSON.parse(localStorage.getItem("ProductList"))
    if (id) {
      Object.assign(this.productList.find(list => list.id === id), { name: product.name, description: product.description, price: product.price });
    } else {
      product['id'] = Math.floor((Math.random() * 6) + 1);
      this.productList.push(product);
    }
    localStorage.setItem('ProductList', JSON.stringify(this.productList));

    return this.http
      .get(this.ADD_OR_EDIT_PRODUCT_API_ENDPOINTS)
      .pipe(
        shareReplay(),
        map((response) => response),
      )
      .toPromise();
  }

  /**
  * @method
  * @name deleteProduct
  * @description used to delete specific product.
  * @return  void
  * @param <number> id
  */
  deleteProduct(id: number): void {
    this.productList = JSON.parse(localStorage.getItem("ProductList"))

    this.productList = this.productList.filter(element => element.id !== id)
    localStorage.setItem('ProductList', JSON.stringify(this.productList));
  }

}
