import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Product } from '../interface';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  title: string = 'product list'
  productList;
  productForm: FormGroup;
  isEdit: boolean = false;
  isOverlayVisible: boolean = true;
  productId;

  constructor(fb: FormBuilder, private userService: UserService) {
    this.productForm = fb.group({
      name: [null, [Validators.required, Validators.pattern(/^[\w\s]+$/)]],
      description: ["", Validators.required],
      price: ["", Validators.required],
    });
  }

  ngOnInit(): void {
    this.getProduct()
  }

  /**
  * @method
  * @name getProduct
  * @description used to get product list.
  * @return  void
  */
  private getProduct(): void {
    this.userService.getProduct().then((res) => {
      this.productList = res;
    })
  }

  /**
  * @method
  * @name onSubmit
  * @description used to submit product form.
  * @return  void
  */
  onSubmit(): void {
    if (this.productForm.valid) {
      this.isOverlayVisible = true
      if (this.isEdit) {
        this.userService.addOrEditProduct(this.productForm.value, this.productId).then((res: any) => {
          window.alert(res?.message);

        }).catch((error) => {
          window.alert(error?.message);

        });
      } else {
        this.userService.addOrEditProduct(this.productForm.value).then((res: any) => {
          window.alert(res?.message);
        }).catch((error) => {
          window.alert(error?.message);
        });
      }
      this.getProduct()
    } else {
      this.productForm.markAllAsTouched();
    }
  }

  /**
  * @method
  * @name editItem
  * @description used to open edit popup
  * @return  void
  * @param <Product> product
  */
  editItem(product: Product): void {
    this.isOverlayVisible = false;
    this.isEdit = true;
    this.productId = product.id;
    this.productForm.get('name').setValue(product.name);
    this.productForm.get('description').setValue(product.description);
    this.productForm.get('price').setValue(product.price)
  }

  /*
  * @method
  * @name delItem
  * @description used to delete product.
  * @return  void
  * @param <Product> product
  */
  delItem(id: number) {
    this.userService.deleteProduct(id);
    this.getProduct();
  }

}
