import { HttpClient, HttpClientModule, HttpHandler } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { UserService } from '../services/user.service';

import { HomePageComponent } from './home-page.component';

describe('HomePageComponent', () => {
  let component: HomePageComponent;
  let fixture: ComponentFixture<HomePageComponent>;
  // let userService: UserService;
  // class MockSortService extends UserService {
  //   deleteProduct() {
  //     return '';
  //   }
  // }
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        FormBuilder, HttpClient, UserService, HttpHandler, HttpClientTestingModule],
      declarations: [HomePageComponent]
    })
      .compileComponents();
  }));

  afterEach(() => {
    TestBed.resetTestingModule();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should call ngOnInit()', () => {
    const OnInitSpy = spyOn(component, "ngOnInit")
    component.ngOnInit();
    expect(OnInitSpy).toHaveBeenCalled();
    expect(true).toBeTrue()
  });

  it('should call delItem()', () => {
    const OnInitSpy = spyOn(component, "delItem").and.callThrough()
    component.delItem(123);
    expect(OnInitSpy).toHaveBeenCalled();
  });


  it('should call editItem()', () => {
    const OnInitSpy = spyOn(component, "editItem").and.callThrough()
    component.editItem({ name: 'test', description: 'test', price: '100$' });
    expect(OnInitSpy).toHaveBeenCalled();
  });

  it('should call onSubmit()', () => {
    const formValue = {
      name: 'test',
      description: 'test',
      price: '100$',
    }
    component.productForm.patchValue(formValue)
    const OnInitSpy = spyOn(component, "onSubmit").and.callThrough()
    component.onSubmit();
    expect(OnInitSpy).toHaveBeenCalled();
  });

  it('should call onSubmit()', () => {
    component.isEdit = true;
    const formValue = {
      name: 'test',
      description: 'test',
      price: '100$',
    }
    component.productForm.patchValue(formValue)
    const OnInitSpy = spyOn(component, "onSubmit").and.callThrough()
    component.onSubmit();
    expect(OnInitSpy).toHaveBeenCalled();
  });

  it('should call onSubmit()', () => {
    const OnInitSpy = spyOn(component, "onSubmit").and.callThrough()
    component.onSubmit();
    expect(OnInitSpy).toHaveBeenCalled();
  });
});
